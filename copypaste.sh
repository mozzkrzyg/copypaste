#!/bin/bash
CLIPBOARD=$(xdg-user-dir DOCUMENTS)/copypaste

if [ ! -f $CLIPBOARD ]; then
    touch -m --date="2000-01-01" $CLIPBOARD 
fi

ACTUALDAYTAG="$(date +"%g%m%d")"
LASTDAYTAG="$(ls $CLIPBOARD -al --time-style=+%y%m%d | grep -o $(date +%y%m%d))"

chmod 600 $CLIPBOARD

if [ "$LASTDAYTAG" != "$ACTUALDAYTAG" ]; then
    printf "\n ____ $ACTUALDAYTAG ____\n" >> $CLIPBOARD
fi

#pastes current hour&minutes and the selected txt
date +"%H:%M" | tr -d '\n' >> $CLIPBOARD
printf " " >> $CLIPBOARD
xclip -o >> $CLIPBOARD
printf "\n" >> $CLIPBOARD

chmod 400 $CLIPBOARD